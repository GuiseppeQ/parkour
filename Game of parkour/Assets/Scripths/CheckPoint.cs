using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckPoint : MonoBehaviour
{
    public Vector3 checkPoint;
    public int fruits;


    private void Start()
    {
        checkPoint = this.transform.position;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Check")
        {
            checkPoint = other.transform.position;
        }

        if (other.transform.tag == "Lava")
        {
             this.transform.position = checkPoint ;
        }

        if (other.transform.tag == "Food")
        {
            fruits++;
            other.gameObject.SetActive(false);
        }

        if(fruits >= 5)
        {
            SceneManager.LoadScene(2);
        }
    }
}
