using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforme : MonoBehaviour
{
    public GameObject plataforme;
    public float velocity;

    private void Update()
    {
        MovePlataforme();
    }

    void MovePlataforme()
    {
        plataforme.transform.Translate(Vector3.up * Time.deltaTime * velocity);
    }
}
